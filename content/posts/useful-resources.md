---
title: "Useful Resources"
date: 2020-08-24T04:00:59+02:00
draft: true
---

I will keep this post updated with the most useful resources I have found.

# Websites

## Computer Graphics

- learnopengl (https://learnopengl.com/) : a really good resource to start messing with OpenGL.
- Inigo Quilez blog (https://www.iquilezles.org/) : incredible resource about procedural synthesis.
- The book of shaders (https://thebookofshaders.com/) : a sadly not finished resource about shaders.

## C++

- cppreference (https://en.cppreference.com/w/) : documentation about all the C++ features.
- isocpp (https://isocpp.org/) : a website grouping good practices to enforce when writing modern C++.

# Books

## Image Processing

- Learning OpenCV 3, A. Kaehler, O'Reilly - A good book covering all the features of the OpenCV library (excepted the latest ones).

## Computer Graphics

- OpenGL Programming Guide, J. Kessenich & Graham Sellers et al., Addison Wesley - A good introduction to OpenGL, this book covers all the functionnalities of the library.

## C++

- Effective C++ (third edition), Scott Meyers, Addison Wesley - A classic book about C++ good practices (prefer the "Effective Modern C++ book from the same author, it is more recent).
- C++ Concurrency in Action, A. Williams, Manning Publications - One of the reference books about concurrency in C++. (still reading it for now).

# Youtube channels

## C++ 

- The Cherno
- C++ Weekly

## Unity

- Brackeys

## Blender 

- Blenderguru
