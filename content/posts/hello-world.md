+++
draft = true
date = 2019-05-27T22:32:28+02:00
title = "hello-world"
description = "My first blogpost, a foreword for this blog"
slug = ""
tags = []
categories = []
externalLink = ""
series = []
+++
# Foreword

I'll expose here the idea behind this blog and what you could find here in the future.

## Programming

Primarly there will be programming stuff here. I'll document my journey through new concepts, technologies or whatever I find interesting during my research.
You can expect to find stuff about Graphics Programming, Video Game development and Real-Time applications.

Some of the material exposed here will be about university projects, and others about my free time projects.

What I plan for now:

* Game physics (see the space shooter game in my portfolio)
* Introduction to Hugo, the framework I use for this website
* Procedural synthesis
* My discovery of Rust and doing Computer Graphics stuff with it

## System administration

One of my hobby is System Administration. One of the specificity of my "home lab" is that I use almost exclusively low powered ARM single board computers as servers (Raspberry Pis).

What I plan for now:

* A series of articles about personnal low power home servers
* Useful automations (backups, file hosting)

## Miscallaneous

Some thing you could find are electronics projects, security.

What I plan for now:

* DIY Artnet wifi dmx controller
* Connected temperature sensors that can be used to optimize home venting/heating 
