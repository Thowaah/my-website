---
title: "Plant Mesh Segmentation"
date: 2019-10-28T17:53:26+01:00
draft: false
illustration: "/portfolio/resources/PlantSegmentation_banner.png"
---

This project was a group project developed during my 1st year of Master's degree. The goal of this project was to design a software that can automatically segment a broad range of plant meshes.

We had to find a solution from scratch. We did read quite a lot of papers about mesh segmentation. It was pretty tough to find a good paper that applied to a similar case. We finally chose a paper that was more on the biological side, that proposed a method for cotton crops phenotyping. We used the first part of the article, that was about segmenting the different parts of the plant, the stem and the leaves. However, we still had some challenges, because cotton crops are fairly straight and this method seemed to work great in this case, but we had to take arbitrary plant meshes, that can be fairly twisted.

## Our take on the method

## Semantic segmentation

The method is fairly simple in the case of cotton crops, you define a cylinder with a fixed radius that is center at the middle of the bottom of the plant (that should be the center of the stem) and will mark all vertices in this cylinder as being part of the stem. The we take a second cylinder with a greater radius and for all vertices we check if their normals are fairly horizontal. This step is used to get all the vertices composing the crop's stem, and excluding the leaves' stems.

At this stage we have two clusters, one is the stem and the other contains all of the leaves. We do have a rough idea of which vertices compose the stem and the leaves, but it is not precise enough. We will need a more precise segmentation.

## Precise segmentation

This method allows use to create multiple clusters that should precisely represent specific elements of the plant, but without knowing if they are part of the stem or the leaves. This is the clustering method implemented in CGAL (Computational Geometry Algorithms Library). We reimplemented it, to better understand how it works, but we could have just used it from CGAL. This clustering gives us regions that  

## Combination of the two methods

Thanks to the two clustering algorithm we have used, we have one rough segmentation, but being semantically accurate, and a second one really precise, but without any clue of which cluster represents what. For every clusters in the precise segmentation, we verify if it contains a majority of vertices representing the stem or the leaves in the semantic representation. Those vertices are then assigned at the corresponding final cluster.

{{% figure src="/portfolio/resources/plantSeg_example.png" %}}

## Results

We have achieved interesting results, close to ideal for some plants, correct for others that still keep a vertical structure, and bad results for others like ivy, which is too twisted for our algorithm.

Here you can see some results

{{% figure src="/portfolio/resources/PlantMeshResults.png" %}}

## References

The two papers used for semantic segmentation:

Paproki, A., Sirault, X., Berry, S., Furbank, R., & Fripp, J. (2012). A novel mesh processing based technique for 3D plant analysis. BMC plant biology, 12(1), 63.
Paproki, A., Fripp, J., Salvado, O., Sirault, X., Berry, S., & Furbank, R. (2011, December). Automated 3D segmentation and analysis of cotton plants. In 2011 International Conference on Digital Image Computing: Techniques and Applications(pp. 555-560). IEEE.

The paper used for the precise segmentation that is implemented in CGAL:

Shapira, L., Shamir, A., & Cohen-Or, D. (2008). Consistent mesh partitioning and skeletonisation using the shape diameter function. The Visual Computer, 24(4), 249.
