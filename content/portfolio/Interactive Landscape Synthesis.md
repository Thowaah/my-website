---
title: "Interactive Landscape Synthesis"
date: 2019-10-04T19:08:58+02:00
weight: 1
draft: false
illustration: "/portfolio/resources/ITS_banner.png"
---

This project is a development project for my last year of Master's degree.

This is the project repository: https://git.unistra.fr/t.weissgerber/projet-150h

## The idea

This project is inspired by an art installation created by Theoriz Studio. You can see it here: https://player.vimeo.com/video/103438556
The goal is to generate in real time a landscape and modulate it using a camera input.
The idea is not to create a precise landscape editing application, but more on the artistic side, to create interaction between a human and the application.

I used procedural techniques on the GPU

The project is composed of 3 modules:
 - the landscape generator
 - the video capture and processing
 - some sound effects

And between those modules some processing that would make the app artistically interesting.

## Landscape generation

I used OpenGL 4.5 to render the landscape.

Here are some techniques that I used for the rendering.

 * I lay down a flat grid of vertices then compute their heights and normals in the shader vertex, using Fractal Brownian Motion
 * The noise is computed using a Compute Shader, so we only need to do a texture lookup to get height and to compute normals 
 * I paint the landscape based on height, and terrain inclination to decide where to put rocks, grass, trees, etc.

## Video processing

For this part I used OpenCV for capturing and processing the images.

I have done a foreground extraction to get the silhouette of the user. Then for a first descriptor I am doing a PCA on the blob. This gives me an orientation measurement for the user. This can then be tied to any parameter of the landscape synthesis.

## Sound processing

This part is really here for the immersion of the user. I wanted to use the FMOD library for this part. This part as not been implemented yet.
