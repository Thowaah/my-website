---
title: "Feature Line Detection"
date: 2019-10-24T11:22:21+02:00
draft: false
illustration: "/portfolio/resources/FeatureLine_banner.png"
---

This was a project I worked on during my first year of Master's degree. The idea was to study the scientific literature around one subject, find the most interesting article and implement it. I worked on feature line detection, and my goal was to implement it in "SCHNApps", a modeling software that is developed by the research team from the ICube lab in Strasbourg, France.

## Article research

Most of the articles about feature line detection were released in the early 2000's. After studying a bunch of articles, I chose an article entitled "Smooth Feature Lines on Surface Meshes", from Klaus Hildebrandt et al.. This article describes a discrete method to compute those lines.

## Implementation

I had to implement this article into a modeling software, to be able to use the computed feature lines as a selection tool.

To begin, I had to compute feature points, with the method described in the paper. With those feature points (2 feature points form a feature line segment), I could pick one feature point randomly, and then check its neighborhood to see if there are other ones, if yes, I repeat the process on it, and if not, I close the current feature line. The result of this is an array of feature lines. Having this data structure allows me to select all points of a feature line and use them to deform the object, with the included tools from the modeling software.

## Results

Here are some pictures of the results.

{{% figure src="/portfolio/resources/fl_hand_example.png" %}}
