---
title: "2D Combat Platformer"
date: 2019-09-29T23:18:58+02:00
draft: false
illustration: "/portfolio/resources/2DGame_banner.png"
---

This game is a mix between a classic combat game and Doodle Jump (an old mobile game) which is a scrolling platform game.

## Context

This game was developed during my last year of bachelor's degree. It was a big group project that involved team management. 

The goal was to create an mobile, online multiplayer game. The online multiplayer part did not work out great. I will be presenting the design of the game in the next section. 

## Functionalities

We created many types of platforms:

* slippery
* destructible
* self-destructible
* trapped
* ghost

We also did put portals on the sides to go from one side to another. We added bonuses, to speed up or down the platform scrolling, lasers, damage reduction...
Damages are like in Super Smash Bros., the more hits you take, the further you go when hit, and if you go too far out, you are eliminated.

## Results

Here is a short video to give you an idea of what the game looked like

{{% youtube HB8BjJFxVNI %}}
