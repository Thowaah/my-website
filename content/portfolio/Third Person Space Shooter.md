---
title: "Third Person Space Shooter"
date: 2019-09-29T23:08:09+02:00
weight: 2
draft: false
illustration: "/portfolio/resources/TPS_banner.png"
---

This game is a third person shooter. Its originality lies on the physics, as the player is on the surface of a really small planet, and the gravity keeps him on it. He can jump from planet to planet if he catches the good trajectory. His projectiles also follows the curvature of the planet and so some interesting shooting skills can be developed.

## Context

This project was developed during my last year of bachelor's degree in a game development course. The idea was to develop a video game from start to finish. We used Unity 3D as the game engine.

I worked mainly on the physics of the game and on the 3D assets. The other members worked on the interface and the multiplayer functionalities.

## State of advancement

The interface and physics part work pretty fine, and also the matchmaking part. The in-game multiplayer functionalities are sadly very buggy. This might be a performance issue with the P2P multiplayer API from Unity. The game is not playable in multiplayer mode due to huge lags. Also, the multiplayer API of Unity that we used is being deprecated.

## The result

Here is a short video to give you an idea of the result

{{% youtube 7SlXuQm5itQ %}}
