+++
draft = false
title = "About me"
description = ""
slug = ""
tags = []
categories = []
externalLink = ""
series = []
+++
# Thomas Weissgerber

## Who I am

I'm currently studying Computer Science at the *Université de Strasbourg*, in Strasbourg, France. My Master's degree is specialized in Computer Graphics and image processing.

I am planning to become an **expert software engineer**, specialized in **real-time applications** and **computer graphics**. I'm aiming towards **creative industries**. I have a particular appeal for the video game industry but I am open to any proposition. 

## Education

I have been interested in computer science and electronic since high school. My first encounter with both worlds (outside of using a computer) was with an Arduino micro-controller.

When choosing what to do after high school, both audio engineering and computer science appealed to me. I ended up choosing computer science, mostly because of the employment rate and stability of the jobs. I applied for a selective training at the University of Strasbourg, specialized in computer graphics, which was a topic that already interested me.

During my training I have learned the basics about computer science (networking, computer architecture, software engineering, system programming, programming languages, concurrency, distributed systems, etc). My training has allowed me to discover some technical fields, like 3D modeling and animations, Unity and also had extra group projects throughout the 5 years, compared to the classic bachelor and master degrees.    

## What I love

Below are the main topics that interest me in Computer Science.

- Game development
- 3D graphics programming
- Image processing
- Procedural generation

## Other things that I have done

Other topics and fields I have studied and in which I have done interesting projects.

- Artificial Intelligence (deep learning)
- Functional programming
- Compiler
- 3D mesh processing
- Rendering
- Networking
- System programming
- Blockchain
- Computer Assisted Design
- Security (ethical hacking)

## Other hobbies

I also have some other hobbies outside of computer science:

- electronics (guitar pedals and micro-controller based projects)
- audio engineering (live audio mixing)
- playing music (guitar, keys, Ableton Live)
- blacksmithing (beginner level stuff, like key rings and bottle openers)
- learning new stuff !

## Programming languages and technologies

I have used the following programming languages (in order of fluency):

- C++ (11 and later)
- C#
- Python
- C
- JavaScript (up to ES6)
- Java

Technologies I worked with:

- Boost
- Computer graphics libraries (OpenGL, OpenCV, PCL)
- Unity


